package com.example.hailstorm;

public class HailStorm {
    static int count;

    // function to print hailstone numbers
    // and to calculate the number of steps
    // required
    static int HailStormNumbers(int num)
    {
        System.out.print(num + " ");

        if (num == 1 && count == 0) {

            // N is initially 1.
            return count;
        }
        else if (num == 1 && count != 0) {

            // N is reduced to 1.
            count++;
            return count;
        }
        else if (num % 2 == 0) {

            // If N is Even.
            count++;
            HailStormNumbers(num / 2);
        }
        else if (num % 2 != 0) {

            // N is Odd.
            count++;
            HailStormNumbers(3 * num + 1);
        }
        return count;
    }

    // Driver code
    public static void main(String[] args)
    {
        int num = 7;
        int steps;

        // Function to generate Hailstorm
        // Numbers
        steps = HailStormNumbers(num);

        // Output: Number of Steps
        System.out.println();
        System.out.println("Number of Steps: " + steps);
    }
}
